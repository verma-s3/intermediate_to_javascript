// Animation .html javascript file

// basic script fot the game.
var the_ball; 
var ball_xpos = 0;
var ball_ypos = 0;
var my_timer;
var xspeed = 10;
var yspeed = 10;
var left_edge = window.innerWidth - 65;
var bottom_edge = window.innerHeight - 65;
var the_paddle;
var mouse_x;
var mouse_y;

window.onresize = function(){
  left_edge = window.innerWidth - 65;
  bottom_edge = window.innerHeight - 65;
}

function movePaddle(event){
  var my_event = window.event;
  
  mouse_x = my_event.clientX;
  mouse_y = my_event.clientY;
  document.getElementById("info").innerHTML = "Mouse X= " + mouse_x + "<br />";
  document.getElementById("info").innerHTML += "Mouse Y= " + mouse_y;
  // don't move th emouse if it is less than bottm edge;
  if(mouse_y < bottom_edge){
    the_paddle.style.top = mouse_y + "px";
  }
}

//detect a movement of a mouse
document.onmousemove = movePaddle;


onload = function(){
  the_ball = document.getElementById("ball");
  the_paddle = document.getElementById("paddle");
  // detect click on the button
  document.getElementById("start_game").onclick = function(){
    // clear the timer, so we don't end up with multiple instances of setIntervel.
    clearTimeout(my_timer);
    document.getElementById("game").style.cursor = "none"; 
    // start the moveBall function 
    moveBall();
  }
}

function moveBall(){
  // increase ball_xposition 
  ball_xpos += xspeed;
  ball_ypos += yspeed;
  
  //what is the current value of the ball xposition if it is more than 1000 set speed negative
  if((ball_xpos > left_edge) || (ball_xpos < 0 )){
    xspeed = -xspeed;
  }
  // what is th ecurrent value - if it is more than the bottom edge set spped to neagtive
  if((ball_ypos > bottom_edge) || (ball_ypos< 0)){
    yspeed = -yspeed;
  }
  
  if(xspeed < 0){
    if(hitTest(the_ball,the_paddle)){
      xspeed = -xspeed;
    }
  }
  // set th eposition of the ball 
  the_ball.style.left = ball_xpos + "px";
  the_ball.style.top = ball_ypos + "px";
  
  // recurse the function.
  my_timer = setTimeout('moveBall()', 17);
  
}