// -----------------function to save the cookie.---------------------

function addCookie(){
  var form_info = document.getElementById("form_data");
  var cookie_array = new Array();
  var end_date = new Date();
  // set the expiry date upto 2 years.
  var ex = end_date.setFullYear(end_date.getFullYear() + 2);
  // loop to add the form elements in a cookie
  for(i=0;i< form_info.elements.length;i++){
    var form_id = form_info.elements[i].id;
    var form_value = form_info.elements[i].value;
    // if statement to save date from input fields.
    if(form_info.elements[i].type != "button"){
      cookie_array[i] = form_id + "::" + form_value;
      // join the cookie array with "--" 
      var form_string = cookie_array.join("--");
    }// END IF statement
  }// ENd Loop
  cookie_string = encodeURIComponent(form_string);
  //alert(cookie_string);
  document.cookie = "form_cookie=" + cookie_string + ";expires=" + end_date.toGMTString();
  if(document.cookie != ''){
    alert("Your Cookie is saved.");
  }
} 

// -----------------------form validation ------------------------
function checkFormValidation(){
  // all data from form in a variable.
  var form_data = document.getElementById("my_form");
  // array to store the values
  var form_values = new Array();
  form_values[0] = document.getElementById("email_address").value;
  form_values[1] = document.getElementById("phone").value;
  form_values[2] = document.getElementById("postal_code").value;

  // array to store name of the form fields.
  var form_fields = new Array();
  form_fields[0] = "Email";
  form_fields[1] = "Phone Number";
  form_fields[2] = "Postal Code";

  // loop for form_value array
  for(i=0;i<form_values.length;i++){
    //if statement to check that form values is empty or not.
    if(form_values[i] == ''){
      alert(form_fields[i] + " is empty, Please Fill " + form_fields[i] + " in the form field");
      return false;
    }//end IF
  }// loop end

  // Check email validation
  var correct_email = checkEmail(document.getElementById("email_address").value);
  if(!correct_email){
    return false;
  }

  // Check phone validation
  var correct_phone_number = checkPhone(document.getElementById("phone").value);
  if(!correct_phone_number){
    return false;
  }

  // check Postalcode
  var correct_postal_code = checkPostal(document.getElementById("postal_code").value);
  if(!correct_postal_code){
    return false;
  }
  addCookie();
}// end of checkFormValidation function.

// function to check email address from the form field
function checkEmail(email_add){
  // loop for checking "@" and "." symbol.
  while((email_add.indexOf("@") < 1) ||
        (email_add.indexOf("@") == email_add.length-1) ||
        (email_add.indexOf("@") == email_add.length-2) ||
        (email_add.indexOf(".") < 1) ||
        (email_add.indexOf(".") == email_add.length-1) ||
        (email_add.indexOf(".") == email_add.length-2) ||
        (email_add.indexOf(' ') != -1)){
    alert("Invalid Email!! Try Again.");  
    return false;
  }// end loop
  return true;
}//end Function of checkEmail

//function to check Phone number from the form field
function checkPhone(mobile_no){
  // split the mobile_no string into array   
  var phoneNo = mobile_no.split("-");
  if((phoneNo.length !=3) || (phoneNo[0].length != 3) || (phoneNo[1].length != 3) || (phoneNo[2].length != 4)){
    alert("must enter the pattern '###-###-####'.");
    return false;
  } 
  for(i=0;i<phoneNo.length;i++){
    if(isNaN(phoneNo[i]) || phoneNo[i].indexOf(" ") != -1){
      alert("No character and space is allowed");
      return false;
    }
  }
  return true;
}//end Function of checkPhone

// function to check postal code from the form field
function checkPostal(Zip_code){
  // if statement to check 6 digit code and nospace
  if((Zip_code.length == 6) && (Zip_code.indexOf(' ') == -1)){
    return true;
  }
  alert("Please Type the correct Zip code and without space");
  return false;        
}// End Function of check Postal Code