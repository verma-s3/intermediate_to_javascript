//----------------------------function to read the cookie.-------------------------------
var split_by_colon;
var split_by_dash ;

function readtheCookie(){

  //first if statement to check cookie is present or empty.
  if(document.cookie != ''){
    // get the cookie from brower
    var get_cookie = document.cookie;
    
    // split the cookie into parts by semi-colon.
    var splitted_cookie = get_cookie.split(";");
    for(i=0;i<splitted_cookie.length;i++){
      if(splitted_cookie[i].indexOf('form_cookie') != -1){
        //decoding of cookie 
        var founded_splitted_cookie = decodeURIComponent(splitted_cookie[i]);
        break;
      }//end of if statement
    }// end Of FOR Loop
    
    // split the cookie again by "=" symbol
    var split_by_equalto = founded_splitted_cookie.split("=");
    var cookie_values = split_by_equalto[1];
    
    // split the cookie by "--".
   split_by_dash = cookie_values.split("--");
    
    // loop to collect the data that splitted bt "--".
    for(i=0;i<split_by_dash.length;i++){
      // split the cookie by colon 
      split_by_colon = split_by_dash[i].split("::");
      // display of data in a form fileds.
      document.getElementById(split_by_colon[0]).value = split_by_colon[1];
    }//end of for loop Statement
    
  }// end of first if statement
  else{
    alert("Sorry! No cookie found.");
  }
}// end of function

//-------------------------function to remove the cookie.--------------------------------------
function removeCookie(cookie_name){
  var end_date = new Date();
  // settimg the date to past year date.
  end_date.setFullYear(end_date.getFullYear() - 1);
  // if statement to check if cookie is not empty the delete it.
  if(document.cookie != ''){
    document.cookie = cookie_name + "=;expires=" + end_date.toGMTString();
    // loop to dlete the form valurs form form as well.
    for(i=0;i<split_by_dash.length;i++){
      // split the cookie by colon 
      split_by_colon = split_by_dash[i].split("::");
      // display of data in a form fileds.
    document.getElementById(split_by_colon[0]).value = "";
    }//end of for loop Statement
    alert("Your Cookie is removed!!");
  }// end IF 
  // otherwise display message of 'no cookie'.
  else{
    alert("No Cookie is present for removal");
  }
}// end of removeCookie function