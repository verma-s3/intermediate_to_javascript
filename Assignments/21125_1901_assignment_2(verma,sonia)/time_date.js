var time_clock;

var month_list_array = new Array();
  month_list_array[0] = "January";
  month_list_array[1] = "Febrary";
  month_list_array[2] = "March";
  month_list_array[3] = "April";
  month_list_array[4] = "May";
  month_list_array[5] = "June";
  month_list_array[6] = "July";
  month_list_array[7] = "August";
  month_list_array[8] = "September";
  month_list_array[9] = "October";
  month_list_array[10] = "November";
  month_list_array[11] = "December";
        
var days_of_week_array = [
  "Sunday",
  "Monday",
  "Tuesday",
  "Wednesday",
  "Thursday",
  "Friday",
  "Saturday"
]

function liveTime(){
  var current_date = new Date();
  var current_year = current_date.getFullYear();
  var current_month = current_date.getMonth();
  var current_date_of_month = current_date.getDate();
  var current_day = current_date.getDay();
  // getting the time in user friendly format.
  var current_mintues = current_date.getMinutes();
  // to make the 2digit number if number is less than 2 
  if(current_mintues < 10){
    current_mintues = "0" + current_mintues;
  }
  
  var current_seconds = current_date.getSeconds();
  // to make the 2digit number if number is less than 2
  if(current_seconds < 10 ){
    current_seconds = "0" + current_seconds;
  }
  
  var current_hours = current_date.getHours();
  // changing the 24hour format to 12 hour format
  if(current_hours > 12 ){
    current_hours -= 12;
    current_seconds = current_seconds + " p.m.";
  }
  if(current_hours == 0){
    current_hours = 12;
    current_seconds = current_seconds + " a.m.";
  }
  
  var current_time = current_date.getTime();
  // display the time on screen by innerhtml method.
  var timer_obj = document.getElementById("display_Time");
  timer_obj.innerHTML = days_of_week_array[current_day] + "  &nbsp;" + 
                        month_list_array[current_month] + " " + 
                        current_date_of_month + ", " + 
                        current_year + " &nbsp; &nbsp; &nbsp; Time is = " + 
                        current_hours + ":" + 
                        current_mintues + ":" + 
                        current_seconds;
}// end of liveTime function

// Onload function to call the liveTIme function when page laods
onload = function(){
  liveTime();
  //setInterval function will call the function itelf again and again
  time_clock = setInterval("liveTime();", 1000);
}// end of Anonymous function
