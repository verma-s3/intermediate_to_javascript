// Animation.html JavaScript file. Basic script for the game. 

var the_ball;
var ball_xpos = 0;
var ball_ypos = 0;
var my_timer; 
var xspeed = 10;
var yspeed = 10;
var left_edge = window.innerWidth - 65;
var bottom_edge = window.innerHeight - 65;
var the_paddle;
var mouse_x;
var mouse_y;
var score = 0;
var gameover = false;

window.onresize = function(){
  left_edge = window.innerWidth - 65;
  bottom_edge = window.innerHeight - 65;
}

function movePaddle(event){
  var my_event = window.event;
  
  mouse_x = my_event.clientX;
  mouse_y = my_event.clientY;
  
  document.getElementById("info").innerHTML = "Mouse X = " + mouse_x + "<br />";
  document.getElementById("info").innerHTML += "Mouse Y = " + mouse_y;
  
  // Don't move the mouse unless it's less than the bottom edge. 
  if(mouse_y < bottom_edge){
    the_paddle.style.top = mouse_y + "px";
  }
}

// Detect a movement of the mouse. 
document.onmousemove = movePaddle;

onload = function(){
  // Get the ID of the DIV, and put it into our "the_ball" variable. 
  the_ball = document.getElementById("ball");
  // Do the same for the paddle. 
  the_paddle = document.getElementById("paddle");

  // Detect click on the button
  document.getElementById("start_game").onclick = function(){
    
    // Clear the timer, so we don't end up with multiple instances of setTimeout. 
    clearTimeout(my_timer);
    document.getElementById("game").style.cursor = "none";
    // Start the moveBall function.
    moveBall();
  }
}




function moveBall(){
  // Increase ball_xpos
  ball_xpos += xspeed;
  ball_ypos += yspeed;
  
  // What is the current value of ball_xpos? If it's more than 1000, set speed to neg. 
  if((ball_xpos > left_edge) || (ball_xpos < 0)){
    // CHeck if ball moving left. 
    if(xspeed < 0){
      score -= 10;
    }
    
    xspeed = -xspeed
    
  }
  // What is the current value ball_ypos if it's more than the bottom_edge, set speed 
  // to negative
  if((ball_ypos > bottom_edge) || (ball_ypos < 0)){
    yspeed = -yspeed;   
  }
  
  // If the ball is moving to the left. 
  if(xspeed < 0){
    // call the "borrowed" hitTest function to check if the objects have intersected. 
    if(hitTest(the_ball,the_paddle)){
      // Reverse the direction of the ball. 
      xspeed = -xspeed;
      // increase the score by 10.
      score += 10;
      // increase speed
      xspeed += 1;
      yspeed += 1;
    }
  }
  
  // Set the position of the ball. 
  the_ball.style.left = ball_xpos + "px";
  the_ball.style.top = ball_ypos + "px";
  
  // update score on page. 
  document.querySelector("#scorebox").innerHTML = score;
  
  // Is the game over?
  if(score < 0){
    gameover = true;  
  }
  
  if(!gameover){
    // Recurse the function. 
    my_timer = setTimeout('moveBall()',17);
  }
  else{
    document.querySelector("#game_over").style.display = "block";
  }
}



